#include <Keypad.h>
#include <LiquidCrystal.h>
#include <EEPROM.h>
#include <SoftwareSerial.h>

#define DEBUG true
SoftwareSerial esp8266(2,3);
const double SOPHUT = 5;
const double SOMILI = SOPHUT*1000*60;
LiquidCrystal lcd(9,8,7,6,5,4);
char password[4];
char pass[4],pass1[4];
int i=0;
unsigned long startSomeone;
unsigned long endSomeone;
unsigned long durationFakeTime = 0;
int remainFakeTime = 0;
unsigned long startFakeTime;
char customKey=0;
int counter;
bool doorLocked=true;
bool hasSomeone = false;
bool openDoor=false;
const byte ROWS = 4; //four rows
const byte COLS = 3; //four columns
char hexaKeys[ROWS][COLS] = {
{'1','2','3'},
{'4','5','6'},
{'7','8','9'},
{'*','0','#'}
};
byte rowPins[ROWS] = {A0,A1,A2,A3}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {A4,A5,13};   //connect to the column pinouts of the keypad
//initialize an instance of class NewKeypad
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);
int led = 10 ;
int PIR = 11 ;
void setup()
{    
  Serial.begin(9600);
  while (!Serial) {
    
  }  
  esp8266.begin(9600);
  counter=0; 
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  lcd.begin(16,2);
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);
  pinMode(PIR,INPUT);
  digitalWrite(PIR, LOW);
//pinMode(buzzer, OUTPUT);
  lcd.print(" Smart Door ");
  //  Serial.print(" Electronic ");
  lcd.setCursor(0,1);
  lcd.print(" Version 1  ");
 // Serial.print(" Keypad Lock ");
  delay(2000);
  lcd.clear();
  lcd.print("Nhap password:");
  Serial.println("Nhap password:");
  lcd.setCursor(0,1);
  //LẦN ĐẦU TIÊN CHẠY THÌ BỎ COMMENT ĐOẠN CODE NÀY ĐỂ NÓ KHỞI TẠO PASSWORD 0000. KỂ LẦN SAU THÌ CỨ COMMENT ĐOẠN CODE NÀY LẠI LÀ ĐƯỢC
//  for (int j = 0; j < 4; j++) {
//  EEPROM.write(j,'1');
//  }
  sendCommand("AT+RST\r\n",2000,DEBUG); // reset module
  sendCommand("AT+CWMODE=1\r\n",1000,DEBUG); // configure as access point
  sendCommand("AT+CWJAP=\"INTERNET-NAM&NGUYET1\",\"bachkhoamottinhyeu\"\r\n",3000,DEBUG);
  delay(10000);
  sendCommand("AT+CIFSR\r\n",1000,DEBUG); // get ip address
  sendCommand("AT+CIPMUX=1\r\n",1000,DEBUG); // configure for multiple connections
  sendCommand("AT+CIPSERVER=1,80\r\n",1000,DEBUG); // turn on server on port 80
  
  Serial.println("Server Ready");  
}

void loop()
{     
  //until someone show up
//  if (digitalRead(PIR)==HIGH&&!hasSomeone) {
//    hasSomeone = true;
//    startSomeone=millis();    
//  }
//  //until someone disappeared
//  else if (digitalRead(PIR)==LOW&&hasSomeone) {
//    endSomeone = millis();
//    unsigned long duringTime=endSomeone-startSomeone;
//    int remainSec = (int) duringTime/1000;
// //   Serial.println(remainSec);
//    hasSomeone=false;
//    thaotung(1,remainSec);
//  }
//  else if (digitalRead(PIR)==HIGH&&hasSomeone) {
//    unsigned long duringTime=millis()-startSomeone;
//    int remainSec =(int) duringTime/1000;
// //   Serial.println(remainSec);
//    thaotung(1,remainSec);
//  }
//  else {
//    thaotung(1,-100);
//  }
  thaotung(1,-100);
  customKey = customKeypad.getKey();
  if (customKeypad.getState()==PRESSED) buzzer();
  if(customKey=='#')
    change();
  if (customKey)
  {
    password[i++]=customKey;
    lcd.print('*');
  //  Serial.print(customKey);
   // beep();
  }
  if(i==4 || openDoor)
  {
    delay(200);
    for(int j=0;j<4;j++)
      pass[j]=EEPROM.read(j);
    if(!(strncmp(password, pass,4))||openDoor)
    {
      counter=0;
      durationFakeTime=0;
      remainFakeTime=0;
      doorLocked=false;
      openDoor=true;
      digitalWrite(led, HIGH);
      //digitalWrite(11,HIGH); 
      lcd.clear();
      lcd.print("Dang mo khoa");
      Serial.println("Dang mo khoa");
      delay(1000);
      lcd.setCursor(0,1);
      lcd.print("#.Thay doi pass");
      Serial.println("#.Thay doi pass");
      delay(1000);
      lcd.clear();
      lcd.println("Cua dang duoc mo");
      Serial.println("Cua dang duoc mo");
      lcd.setCursor(0,1);
      lcd.println("*.De dong cua");
      Serial.println("*.De dong cua");
      //digitalWrite(11,LOW);  
      unsigned long startTime=millis();   
      lcd.clear();
      lcd.println("Tu dong cua sau:");
      Serial.println("Tu dong cua sau:");   
      while(true) {        
        customKey = customKeypad.getKey();
        unsigned long currentTime=millis();
        unsigned long duration=currentTime-startTime; 
        unsigned long remainTime=(SOMILI-duration)/1000;               
        int remainSec=(int) remainTime%60;
        int remainMin=(int) remainTime/60;
        lcd.setCursor(0,1);
        thaotung(2,remainTime);
        if (remainMin>0) {          
          lcd.println(remainMin);
          lcd.println("phut");
          //Serial.println(remainMin);
          //Serial.println(" phut ");
          lcd.println(remainSec);
          lcd.println("giay");
          //Serial.println(remainSec);
          //Serial.println(" giay");
        }
        else {
          lcd.println(remainSec);
          lcd.println("giay");
          //Serial.println(remainSec);
          //Serial.println(" giay");
        }
        if (customKey=='*' || remainTime==0||!openDoor) {
          lcd.clear();
          lcd.println("Cua dang dong");
          Serial.println("Cua dang dong");
          lcd.setCursor(0,1);  
          lcd.println("#. De doi pass");
          Serial.println("#. De doi pass");
          i=0;        
          //digitalWrite(12,HIGH);
          delay(2000);
          digitalWrite(led, LOW);
          //digitalWrite(12,LOW);  
          doorLocked=true;
          break;
        }
      }
      lcd.clear();
      lcd.print("Nhap pass:");
      Serial.println("Nhap pass:");
      lcd.setCursor(0,1);
      openDoor=false;
    }
    else
    {
      lcd.clear();
      lcd.print("Sai pass,nhap lai");
      Serial.println("Sai pass,nhap lai");
      counter++;
      if (counter>2) {
        for(int j =1;j<5;j++) {
          blink();
          delay(500);          
        }
        if (counter==3) startFakeTime=millis();
      }
      lcd.setCursor(0,1);
      lcd.print("#.Thay doi pass");
      Serial.println("#.Thay doi pass");
      delay(2000);
      lcd.clear();
      lcd.print("Nhap pass:");
      Serial.println("Nhap pass:");
      lcd.setCursor(0,1);
      i=0;
    }
  }  
}

void thaotung(int i,int remainSec) {
  if(esp8266.available()) // check if the esp is sending a message 
  {    
    if(esp8266.find("+IPD,"))
    {
     delay(1000); // wait for the serial buffer to fill up (read all the serial data)
     // get the connection id so that we can then disconnect
     int connectionId = esp8266.read()-48; // subtract 48 because the read() function returns 
                                           // the ASCII decimal value and 0 (the first decimal number) starts at 48
     
     esp8266.find("pin="); // advance cursor to "pin="
          
     int pinNumber = (esp8266.read()-48); // get first number i.e. if the pin 13 then the 1st number is 1

     esp8266.find("password=");
     String pass_ex = esp8266.readStringUntil(" ");
     for(int j=0;j<4;j++)
      pass[j]=EEPROM.read(j);
     Serial.println(!strncmp(pass_ex.c_str(),pass,4));
     // build string that is send back to device that is requesting pin toggle
     if (strncmp(pass_ex.c_str(),pass,4)) {
       String content="-200;-200;-200;";
       sendHTTPResponse(connectionId,content);
       
       // make close command
       String closeCommand = "AT+CIPCLOSE=";
       closeCommand+=connectionId; // append connection id
       closeCommand+="\r\n";
       
       sendCommand(closeCommand,1000,DEBUG); // close connection
     }
     //Btn nhan mo cua
     if (pinNumber==1&&!strncmp(pass_ex.c_str(),pass,4)) {
       String content;
       if (remainSec==-100) {
           content="1;300;2;";           
       }
       else 
           content="1;"+String (remainSec)+";2;";
       sendHTTPResponse(connectionId,content);
       
       // make close command
       String closeCommand = "AT+CIPCLOSE=";
       closeCommand+=connectionId; // append connection id
       closeCommand+="\r\n";
       
       sendCommand(closeCommand,1000,DEBUG); // close connection
       openDoor=true; 
     }
     //Btn dong cua
     if (pinNumber==2&&!strncmp(pass_ex.c_str(),pass,4)) {
       String content="2;-100;2;";
       sendHTTPResponse(connectionId,content);
       
       // make close command
       String closeCommand = "AT+CIPCLOSE=";
       closeCommand+=connectionId; // append connection id
       closeCommand+="\r\n";
       
       sendCommand(closeCommand,1000,DEBUG); // close connection
       openDoor=false; 
     }
     //Btn kiem tra ket noi
     if (pinNumber==3&&!strncmp(pass_ex.c_str(),pass,4)) {
       String content;
       if (i==1 && remainSec==-100) {
         if (counter < 3) {
           content="2;-100;2;";
         }         
         else {           
           durationFakeTime = millis() - startFakeTime;
           remainFakeTime = durationFakeTime/1000;
           content="2;-100;"+(String) remainFakeTime+";";
         }
       }
       else if (i==1 && remainSec!=-100) {
         content="2;"+String (remainSec)+";1;";
       }
       else if (i==2 && remainSec!=-100) {
         content="1;"+String (remainSec)+";2;";
       }
       sendHTTPResponse(connectionId,content);       
       // make close command
       String closeCommand = "AT+CIPCLOSE=";
       closeCommand+=connectionId; // append connection id
       closeCommand+="\r\n";
       
       sendCommand(closeCommand,1000,DEBUG); // close connection       
     }     
    }
  }
}

void change()
{
  int j=0;
  lcd.clear();
  lcd.print("Pass hien tai:");
  lcd.setCursor(0,1);
  while(j<4)
  {
    char key=customKeypad.getKey();
    if (customKeypad.getState()==PRESSED) buzzer();
    if(key)
    {
      pass1[j++]=key;
      lcd.print('*');
    }
    key=0;
  }
  delay(500);

  if((strncmp(pass1, pass, 4)))
  {
    lcd.clear();
    lcd.print("Sai pass");
    lcd.setCursor(0,1);
    lcd.print("May man lan sau");
    delay(1000);
  }
  else
  {
    j=0;
    lcd.clear();
    lcd.print("Nhap pass moi:");
    lcd.setCursor(0,1);
    while(j<4)
    {
      char key=customKeypad.getKey();
      if (customKeypad.getState()==PRESSED) buzzer();
      if(key)
      {
        pass[j]=key;
        lcd.print('*');
        EEPROM.write(j,key);
        j++;
      }
    }
    lcd.print(" Done......");
    delay(1000);
  }
  lcd.clear();
  lcd.print("Nhap pass:");
  lcd.setCursor(0,1);
  customKey=0;
  
}

void blink() {
  digitalWrite(led,HIGH);
  delay(500);
  digitalWrite(led,LOW);
}

String sendData(String command, const int timeout, boolean debug)
{
    String response = "";
    
    int dataSize = command.length();
    char data[dataSize];
    command.toCharArray(data,dataSize);
           
    esp8266.write(data,dataSize); // send the read character to the esp8266
    if(debug)
    {
      Serial.println("\r\n====== HTTP Response From Arduino ======");
      Serial.write(data,dataSize);
      Serial.println("\r\n========================================");
    }
    
    long int time = millis();
    
    while( (time+timeout) > millis())
    {
      while(esp8266.available())
      {
        
        // The esp has data so display its output to the serial window 
        char c = esp8266.read(); // read the next character.
        response+=c;
      }  
    }
    
    if(debug)
    {
      Serial.print(response);
    }
    
    return response;
}
 
/*
* Name: sendHTTPResponse
* Description: Function that sends HTTP 200, HTML UTF-8 response
*/
void sendHTTPResponse(int connectionId, String content)
{
     
     // build HTTP response
     String httpResponse;
     String httpHeader;
     // HTTP Header
     httpHeader = "HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\r\n"; 
     httpHeader += "Content-Length: ";
     httpHeader += content.length();
     httpHeader += "\r\n";
     httpHeader +="Connection: close\r\n\r\n";
     httpResponse = httpHeader + content + " "; // There is a bug in this code: the last character of "content" is not sent, I cheated by adding this extra space
     sendCIPData(connectionId,httpResponse);
}
 
/*
* Name: sendCIPDATA
* Description: sends a CIPSEND=<connectionId>,<data> command
*
*/
void sendCIPData(int connectionId, String data)
{
   String cipSend = "AT+CIPSEND=";
   cipSend += connectionId;
   cipSend += ",";
   cipSend +=data.length();
   cipSend +="\r\n";
   sendCommand(cipSend,1000,DEBUG);
   sendData(data,1000,DEBUG);
}
 
/*
* Name: sendCommand
* Description: Function used to send data to ESP8266.
* Params: command - the data/command to send; timeout - the time to wait for a response; debug - print to Serial window?(true = yes, false = no)
* Returns: The response from the esp8266 (if there is a reponse)
*/
String sendCommand(String command, const int timeout, boolean debug)
{
    String response = "";
           
    esp8266.print(command); // send the read character to the esp8266
    
    long int time = millis();
    
    while( (time+timeout) > millis())
    {
      while(esp8266.available())
      {
        
        // The esp has data so display its output to the serial window 
        char c = esp8266.read(); // read the next character.
        response+=c;
      }  
    }
    
    if(debug)
    {
      Serial.print(response);
    }    
    return response;
} 
void buzzer() {
    digitalWrite(led,HIGH);
    delay(20);
    digitalWrite(led,LOW);
}
