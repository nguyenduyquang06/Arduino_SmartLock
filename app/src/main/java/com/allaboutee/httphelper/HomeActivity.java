package com.allaboutee.httphelper;

import java.util.Timer;
import java.util.TimerTask;
import android.os.*;
import android.media.ToneGenerator;
import android.app.Activity;
import android.media.AudioManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.os.CountDownTimer;
import android.graphics.Color;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

public class HomeActivity extends Activity implements View.OnClickListener {

    public final static String PREF_IP = "PREF_IP_ADDRESS";
    public final static String PREF_PORT = "PREF_PORT_NUMBER";
    public final static String PREF_PASSWORD = "PREF_PASSWORD";
    // declare buttons and text inputs
    private Button buttonPin11,buttonPin12,buttonPin13;
    private EditText editTextIPAddress, editTextPortNumber, editTextPassword;
    private TextView ketnoi1,ketnoi2,ketnoi3;
    // shared preferences objects used to save the IP address and port so that the user doesn't have to
    // type them next time he/she opens the app.
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        sharedPreferences = getSharedPreferences("HTTP_HELPER_PREFS",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        // assign buttons
        buttonPin11 = (Button)findViewById(R.id.buttonPin11);
        buttonPin12 = (Button)findViewById(R.id.buttonPin12);
        buttonPin13 = (Button)findViewById(R.id.buttonPin13);

        // assign text inputs
        editTextIPAddress = (EditText)findViewById(R.id.editTextIPAddress);
        editTextPortNumber = (EditText)findViewById(R.id.editTextPortNumber);
        editTextPassword = (EditText)findViewById(R.id.editTextPassword);

        // assign text views outputs
        ketnoi1=(TextView)findViewById(R.id.isDoorOpened);
        ketnoi2=(TextView)findViewById(R.id.remainSecond);
        ketnoi3=(TextView)findViewById(R.id.isDoorAlerted);
        ketnoi1.setTextColor(Color.parseColor("#FF5722"));
        ketnoi2.setTextColor(Color.parseColor("#FF5722"));
        ketnoi3.setTextColor(Color.parseColor("#FF5722"));
        // set button listener (this class)
        buttonPin11.setOnClickListener(this);
        buttonPin12.setOnClickListener(this);
        buttonPin13.setOnClickListener(this);

        // get the IP address and port number from the last time the user used the app,
        // put an empty string "" is this is the first time.
        editTextIPAddress.setText(sharedPreferences.getString(PREF_IP,""));
        editTextPortNumber.setText(sharedPreferences.getString(PREF_PORT,""));
        editTextPassword.setText(sharedPreferences.getString(PREF_PASSWORD,""));
    }


    public void callAsynchronousTask() {
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        // get the ip address
                        String ipAddress = editTextIPAddress.getText().toString().trim();
                        // get the port number
                        String portNumber = editTextPortNumber.getText().toString().trim();
                        //get password
                        String passDoor = editTextPassword.getText().toString().trim();
                        new HttpRequestAsyncTask(
                                "3", ipAddress, portNumber, passDoor, "pin"
                        ).execute();
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 5000); //execute in every 5000 ms
    }

    @Override
    public void onClick(View view) {

        // get the pin number
        String parameterValue = "";
        // get the ip address
        String ipAddress = editTextIPAddress.getText().toString().trim();
        // get the port number
        String portNumber = editTextPortNumber.getText().toString().trim();
        //get password
        String passDoor = editTextPassword.getText().toString().trim();

        // save the IP address and port for the next time the app is used
        editor.putString(PREF_IP,ipAddress); // set the ip address value to save
        editor.putString(PREF_PORT,portNumber); // set the port number to save
        editor.putString(PREF_PASSWORD,passDoor);
        editor.commit(); // save the IP and PORT

        // get the pin number from the button that was clicked
        if(view.getId()==buttonPin11.getId())
        {
            parameterValue = "1";
        }
        else if(view.getId()==buttonPin12.getId())
        {
            parameterValue = "2";
        }
        else if(view.getId()==buttonPin13.getId())
        {
            parameterValue = "3";
        }
        // execute HTTP request
        if(ipAddress.length()>0 && portNumber.length()>0) {
            new HttpRequestAsyncTask(
                    parameterValue, ipAddress, portNumber, passDoor, "pin"
            ).execute();
        }
        buttonPin13.setPressed(false);
        buttonPin13.invalidate();
        Handler handler1 = new Handler();
        Runnable r1 = new Runnable() {
            public void run() {
                buttonPin13.performClick();
                buttonPin13.setPressed(true);
                buttonPin13.invalidate();
                buttonPin13.setPressed(false);
                buttonPin13.invalidate();
            }
        };
        handler1.postDelayed(r1, 30000);
    }

    /**
     * Description: Send an HTTP Get request to a specified ip address and port.
     * Also send a parameter "parameterName" with the value of "parameterValue".
     * @param parameterValue the pin number to toggle
     * @param ipAddress the ip address to send the request to
     * @param portNumber the port number of the ip address
     * @param parameterName
     * @return The ip address' reply text, or an ERROR message is it fails to receive one
     */
    public String sendRequest(String parameterValue, String ipAddress, String portNumber, String passDoor, String parameterName) {
        String serverResponse = "ERROR";

        try {

            HttpClient httpclient = new DefaultHttpClient(); // create an HTTP client
            // define the URL e.g. http://myIpaddress:myport/?pin=13&password=1234 (to toggle pin 13 for example)
            URI website = new URI("http://"+ipAddress+":"+portNumber+"/?"+parameterName+"="+parameterValue+"&password="+passDoor);
            HttpGet getRequest = new HttpGet(); // create an HTTP GET object
            getRequest.setURI(website); // set the URL of the GET request
            HttpResponse response = httpclient.execute(getRequest); // execute the request
            // get the ip address server's reply
            InputStream content = null;
            content = response.getEntity().getContent();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    content
            ));
            serverResponse = in.readLine();
            // Close the connection
            content.close();
        } catch (ClientProtocolException e) {
            // HTTP error
            serverResponse = e.getMessage();
            e.printStackTrace();
        } catch (IOException e) {
            // IO error
            serverResponse = e.getMessage();
            e.printStackTrace();
        } catch (URISyntaxException e) {
            // URL syntax error
            serverResponse = e.getMessage();
            e.printStackTrace();
        }
        // return the server's reply/response text
        return serverResponse;
    }


    /**
     * An AsyncTask is needed to execute HTTP requests in the background so that they do not
     * block the user interface.
     */


    private class HttpRequestAsyncTask extends AsyncTask<Void, Void, Void> {

        // declare variables needed
        private String requestReply,ipAddress, portNumber, passDoor;
        private String parameter;
        private String parameterValue;


        /**
         * Description: The asyncTask class constructor. Assigns the values used in its other methods.
         //* @param context the application context, needed to create the dialog
         * @param parameterValue the pin number to toggle
         * @param ipAddress the ip address to send the request to
         * @param portNumber the port number of the ip address
         */
        public HttpRequestAsyncTask(String parameterValue, String ipAddress, String portNumber,String passDoor, String parameter)
        {
            this.ipAddress = ipAddress;
            this.parameterValue = parameterValue;
            this.portNumber = portNumber;
            this.passDoor = passDoor;
            this.parameter = parameter;
        }

        /**
         * Name: doInBackground
         * Description: Sends the request to the ip address
         * @param voids
         * @return
         */
        @Override
        protected Void doInBackground(Void... voids) {
            requestReply = sendRequest(parameterValue,ipAddress,portNumber, passDoor, parameter);
            return null;
        }

        /**
         * Name: onPostExecute
         * Description: This function is executed after the HTTP request returns from the ip address.
         * The function sets the dialog's message with the reply text from the server and display the dialog
         * if it's not displayed already (in case it was closed by accident);
         * @param aVoid void parameter
         */
        @Override
        protected void onPostExecute(Void aVoid) {
            String[] separated = requestReply.split(";");
            int isDoorOpened = Integer.parseInt(separated[0]);
            int remainSecond=Integer.parseInt(separated[1]);
            int isDoorAlerted=Integer.parseInt(separated[2]);
            boolean isLoop;

            if (isDoorOpened==0&&isDoorAlerted==0&&remainSecond==-100) {
                // do sth
            }
            else {
                if (isDoorOpened==1) {
                    ketnoi1.setText("Cửa đang mở");      //Đã kết nối
                    ketnoi1.setTextColor(Color.parseColor("#00E676"));
                }
                else if (isDoorOpened==2) {
                    ketnoi1.setText("Cửa đang đóng");   //Đã kết nối
                    ketnoi1.setTextColor(Color.parseColor("#00E676"));
                    isLoop=false;
                }
                else if (isDoorOpened==-200) {
                    ketnoi1.setText("Sai pass, yêu cầu nhập lại");
                    ketnoi1.setTextColor(Color.parseColor("#FF5722"));
                    isLoop=false;
                }
                else {
                    ketnoi1.setText("Chưa kết nối");
                    ketnoi1.setTextColor(Color.parseColor("#FF5722"));
                    isLoop=false;
                }
                if (isDoorOpened==2) {
                    ketnoi2.setText("Cửa đang đóng");
                    ketnoi2.setTextColor(Color.parseColor("#00E676"));
                }
                else if (isDoorOpened==-200) {
                    ketnoi2.setText("Sai pass, yêu cầu nhập lại");
                    ketnoi2.setTextColor(Color.parseColor("#FF5722"));
                }
                else {
                    if (remainSecond>60){
                        int remainMinute= remainSecond/60;
                        ketnoi2.setText(remainMinute + " phút " + (int) (remainSecond - remainMinute*60) + " giây");
                        ketnoi2.setTextColor(Color.parseColor("#00E676"));
                    }
                    else {
                        ketnoi2.setText(remainSecond+" giây");
                        ketnoi2.setTextColor(Color.parseColor("#00E676"));
                    }
                }
/*
                if (isDoorAlerted==1) {
                    ketnoi3.setText("Đã nhập sai quá 3 lần");
                    ketnoi3.setTextColor(Color.parseColor("#C62828"));
                }
*/
                if (isDoorAlerted==2) {
                    ketnoi3.setText("Chưa báo động");
                    ketnoi3.setTextColor(Color.parseColor("#00E676"));
                }
                else if (isDoorOpened==-200) {
                    ketnoi3.setText("Sai pass, yêu cầu nhập lại");
                    ketnoi3.setTextColor(Color.parseColor("#FF5722"));
                }
                else {
                    ketnoi3.setText(isDoorAlerted+" giây từ khi nhập sai pass 3 lần");
                    ketnoi3.setTextColor(Color.parseColor("#FF5722"));
                    ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
                    toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP,3000);
                }
            }
        }

        /**
         * Name: onPreExecute
         * Description: This function is executed before the HTTP request is sent to ip address.
         * The function will set the dialog's message and display the dialog.
         */
        @Override
        protected void onPreExecute() {

        }

    }
}
